# Pitch In

There are a number of ways members of our community can help.  This project is gaining focus, and as it does, the opportunities will multiply rapidly.

We are actively seeking people knowledge able about how to use privacy respecting software to write educational material (or develop content in another medium) to show the average computer user how to protect their privacy while communicating.

Some possible topics
    
    * explain in non-technical terms how the technology used in the FreedomBox software collection works (Tor, VPN, PGP, SSL client certificate authentication)
    * encrypted email
    * private web browsing (blocking tracking)
    * self hosting communications services (instant messaging, email)
    * using a VPN
    * good password management hygiene

## Volunteers

The FreedomBox software collection is under active development but new contributors are always welcome. The software collection team primarily communicates using the [[FreedomBox Mailing
list|http://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss]] and on the [#freedombox IRC channel|irc://irc.oftc.net/freedombox] on [[OFTC|http://www.oftc.net/]]. We also have monthly [[progress "calls"|https://wiki.debian.org/FreedomBox/ProgressCalls]] to get the whole team together to talk. People interested in contributing often start by listening in on a progress call. We have [[instructions|https://wiki.debian.org/FreedomBox/ProgressCalls#Access]] posted for participating in the progress calls. No password or pre-approval is required just connect to call and start contributing!

We have a list of [[current tasks|https://wiki.debian.org/FreedomBox/TODO]] that reflect the current priorities for organizing and including easy to use privacy respecting software into the collection.  Read [[Debian's wiki pages about FreedomBox|http://wiki.debian.org/FreedomBox/]] to learn more about [[contributing|http://wiki.debian.org/FreedomBox/Contribute]].

## [[Donate]]

The FreedomBox Foundation is a Delaware non-profit that relies on community support to continue supporting the FreedomBox social and technical movements. If you are able to help financially, please visit our [[donate]] page.

