# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# lkppo, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2014-11-25 21:23+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: French (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"FreedomBox at LinuxConf North America\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "FreedomBox at LinuxConf North America"
msgstr ""

#. type: Plain text
msgid ""
"FreedomBox Foundation's founder Eben Moglen and Tech Leader Bdale Garbee "
"will be attending the next Linux Conference North America in Vancouver, Aug "
"17-19. This year's edition marks the 20th anniversary of Linux kernel, a "
"major milestone for the community."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Bdale Garbee will speak on Wed Aug 17th at 3pm in Plaza B. \n"
"<ul>\n"
"<li><a href=\"http://events.linuxfoundation.org/events/linuxcon/garbee\">Freedom, Out of the Box! by Bdale Garbee</a><br />a status update on the development of \"FreedomBox\", a personal server running a free software operating system and free applications, designed to create and preserve personal privacy by providing a secure platform upon which federated social networks can be constructed.</li>\n"
"</ul>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Prof. Eben Moglen will speak at the panel <a href=\"http://events.linuxfoundation.org/events/linuxcon/20-years-of-linux-panel\">20 years of Linux</a> right after Bdale's speech and he will available also during other social events.\n"
"Follow us on <a href=\"http://identi.ca/freedomboxfndn\">identi.ca</a>/<a href=\"http://twitter.com/freedomboxfndn\">twitter</a> to get last minute announcements.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr "[[!sidebar  content=\"\"\"\n"

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr "[[Faire un don]]"
