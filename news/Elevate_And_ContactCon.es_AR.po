# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-11-04 18:52+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Spanish (Argentina) (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/es_AR/)\n"
"Language: es_AR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Elevate and ContactCon\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Elevate and ContactCon"
msgstr ""

#. type: Plain text
msgid ""
"James Vasile attended Douglas Rushkoff's ContactCon to promote the "
"FreedomBox.  Thanks to Douglas Rushkoff and Venessa Miemis for inviting us "
"to present and producing the event! My talk there was [The FreedomBox in 4 "
"Minutes](http://freedomboxfndn.mirocommunity.org/video/8/what-is-freedombox-"
"contact-con).  He didn't just go to talk, the FreedomBox project won a prize "
"at ContactCon, too! We'll have a full announcement about that soon."
msgstr ""

#. type: Plain text
msgid ""
"James headed straight from ContactCon to Austria's Elevate festival.  While "
"there, he hopped over to MAMA/Hacklab in Zagreb and also presented the "
"FreedomBox in Ljubljana.  Elevate was packed with great technology, media "
"and arts events.  Many thanks to Daniel Erlacher for the invitation and to "
"Elevate for their donation to the FreedomBox Foundation.  James's Elevate "
"talk was called [Freedom Out of the Box](http://freedomboxfndn.mirocommunity."
"org/video/9/elevate-2011)."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""
