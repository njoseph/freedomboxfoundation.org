[[!meta title="Announcing Freedombox-Privoxy"]]

# Enhanced Privacy and Security for Web Browsing

One thing many people agree the FreedomBox should do is web filtering
for privacy and ad-removal.  Toward that end, the FreedomBox will act
as a web proxy to clean up and protect web traffic.

We have a first draft version of privoxy
[up on git](https://github.com/jvasile/freedombox-privoxy).  It
upgrades your web traffic to prefer ssl encryption whereever it can.
It also strips tracking software from web pages to give you greater
privacy and anonymity as you surf.

If you are a privoxy user, please do give this package a test run and
report any problems on the
[issue tracker](https://github.com/jvasile/freedombox-privoxy/issues).
We are working on upstreaming these changes to the privoxy project,
and in the mean time, you can make a debian package quite easily from
the git repository.

Further work will include writing a script to test all the
https-everywhere rules and discard the ones that are broken.  As well
as one to periodically check for new regexes.  Anybody who wants to
contribute to writing that is welcome to jump on in!

More details about this part of FreedomBox can be found on
[our code page](https://www.freedomboxfoundation.org/code/).

[[!sidebar  content="""

Links:
[[Home|https://www.freedomboxfoundation.org/]]  
[[FAQ]]  
[[Donate]]

"""]]
