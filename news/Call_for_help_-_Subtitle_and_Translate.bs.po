# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2012-03-01 19:34+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
msgid ""
"The FreedomBox Foundation is just at the beginning of our efforts to "
"collaborate with a worldwide community.  We need to speak to everybody, "
"everywhere, in their native language.  That's why we're building translation "
"into the project from the beginning."
msgstr ""

#. type: Plain text
msgid ""
"Since most of our materials start out as video, the first step towards "
"translation is transcription.  If you speak English and want to help spread "
"the message to a wider audience, please stop by the [[subtitle]] page to see "
"what material needs transcription."
msgstr ""

#. type: Plain text
msgid ""
"If you speak any other languages in addition to English, we need your help "
"on our translation team. We are organizing language-based groups to "
"collaborate on high quality translations of project materials, both the "
"transcribed videos and the various web pages and news items on the "
"foundation's site.  If you are interested, please email "
"<join@freedomboxfoundation.org> or stop by the [[translate]] page for more "
"details."
msgstr ""

#. type: Plain text
msgid ""
"We'll provide mailing lists and other communication tools to help make "
"collaboration as easy as possible but we can't do this without your help."
msgstr ""

#. type: Plain text
msgid ""
"So email the FreedomBox Foundation at <join@freedomboxfoundation.org>.  Tell "
"us what languages you can help with.  We'll put together teams."
msgstr ""
