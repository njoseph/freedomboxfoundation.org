# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-10-14 15:30+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "##Freedombox 0.5 Released!\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*August 7, 2015*\n"
msgstr ""

#. type: Plain text
msgid ""
"We are pleased to announce that FreedomBox version 0.5 has been released! "
"This release comes 7 months after the previous, 0.3 release."
msgstr ""

#. type: Plain text
msgid "The FreedomBox version 0.5 is available here:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "\thttp://ftp.skolelinux.org/pub/freedombox/0.5/\n"
msgstr ""

#. type: Plain text
msgid ""
"Before using, you should verify the image's signature, see https://wiki."
"debian.org/FreedomBox/Download for further instructions:"
msgstr ""

#. type: Plain text
msgid ""
"$ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys "
"0x36C361440C9BC971"
msgstr ""

#. type: Plain text
msgid "$ gpg --fingerprint 0x36C361440C9BC971"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    pub   4096R/0C9BC971 2011-11-12\n"
"          Key fingerprint = BCBE BD57 A11F 70B2 3782\n"
"                            BC57 36C3 6144 0C9B C971\n"
"    uid                  Sunil Mohan Adapa <sunil@medhas.org>\n"
"    sub   4096R/4C1D4B57 2011-11-12\n"
msgstr ""

#. type: Plain text
msgid ""
"$ gpg --verify freedombox-unstable_2015-08-06_raspberry-armel-card.tar.bz2."
"sig freedombox-unstable_2015-08-06_raspberry-armel-card.tar.bz2"
msgstr ""

#. type: Plain text
msgid "(Replace the file names with the version you download.)"
msgstr ""

#. type: Plain text
msgid "Thanks to all who helped put this release together."
msgstr ""

#. type: Plain text
msgid "More information on this release is available on the wiki:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "\thttps://wiki.debian.org/FreedomBox/ReleaseNotes\n"
msgstr ""

#. type: Plain text
msgid "Major FreedomBox 0.5 Changes:"
msgstr ""

#. type: Plain text
msgid "- New targets: CubieTruck, i386, amd64"
msgstr ""

#. type: Plain text
msgid ""
"- New apps in Plinth: Transmission, Dynamic DNS, Mumble, ikiwiki, Deluge, "
"Roundcube, Privoxy"
msgstr ""

#. type: Plain text
msgid ""
"- NetworkManager handles network configuration and can be manipulated "
"through Plinth."
msgstr ""

#. type: Plain text
msgid ""
"- Software Upgrades (unattended-upgrades) module can upgrade the system, and "
"enable automatic upgrades."
msgstr ""

#. type: Plain text
msgid ""
"- Plinth is now capable of installing ejabberd, jwchat, and privoxy, so they "
"are not included in image but can be installed when needed."
msgstr ""

#. type: Plain text
msgid ""
"- User authentication through LDAP for SSH, XMPP (ejabberd), and ikiwiki."
msgstr ""

#. type: Plain text
msgid ""
"- Unit test suite is automatically run on Plinth upstream. This helps us "
"catch at least some code errors before they are discovered by users!"
msgstr ""

#. type: Plain text
msgid "- New, simpler look for Plinth."
msgstr ""

#. type: Plain text
msgid "- Performance improvements for Plinth."
msgstr ""

#. type: Plain text
msgid ""
"Please feel free to join us to discuss this release on the mailing list, "
"IRC, or on the monthly progress calls:"
msgstr ""

#. type: Plain text
msgid "- List: http://lists.alioth.debian.org/pipermail/freedombox-discuss/"
msgstr ""

#. type: Plain text
msgid "- IRC: irc://irc.debian.org/freedombox"
msgstr ""

#. type: Plain text
msgid "- Calls: https://wiki.debian.org/FreedomBox/ProgressCalls"
msgstr ""
