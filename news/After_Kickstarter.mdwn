# After Kickstarter

We've completed our Community Angel round of Kickstarter funding, and
I want to thank everybody who donated, spread the word, evangelized
and joined the discussion.  It is amazing to see a worldwide community
coalesce around supporting Freedom and developing this technology.

## Current Activity and Plans

We are still searching for a community relations facilitator.  We have
a number of resumes and are starting to do some interviews.  It's a
slow process, and more great people apply every day.

We continue to seek further funding to fill out the rest of our
budget.

We've started to brainstorm a roadmap at
<http://wiki.debian.org/FreedomBox/UserRequirements/BrainStorm>.  I
took the results of that page and am boiling it down to essential
project goals.  That will be published for a round of community
hacking, which will help us define exactly what this project intends
to do within the broad mandate of FreedomBox possibilities.

Bdale Garbee is slowly enfolding the large amount of technical work
ahead of us into his grasp.  As the technical advisory committee gets
up to steam and a roadmap coheres, we will start to make real
progress.

I attended Libre Planet and spent a weekend waving the FreedomBox flag
and inviting people to join our growing effort.  It was a wonderful
weekend.  I encourage anybody attending conferences to do lightning
talks about the project.  There's a lot to talk about and the response
will be terrific.

Our translation team is now over 50-strong and making short work of
our existing [media page](http://freedomboxfndn.mirocommunity.org/).
It has been a great deal of effort undertaken with grace and good
humor.  Thanks to all who have helped!  If you want to join the fun,
sign up to the
[translation list](https://lists.freedomboxfoundation.org/s/info/i18n)
and introduce yourself.

We plan to form further teams around non-technical aspects of this
project.  These teams will manage documentation, user support, public
outreach, conferences and the like.

## T-Shirt Designs

One of the things we offered Kickstarter donors was a T-shirt.  We are
thus soliciting T-shirt designs.  The theme of the shirt is Community
Angels.  It would be good to involve our [logo](http://wiki.debian.org/FreedomBox?action=AttachFile&do=get&target=freedombox.svg), which was designed by [Luka Marčetić](http://arka.foi.hr/~lmarcetic/).

The chosen design will be printed on t-shirts that we will give to
donors.  Of course, if your design is chosen, we will cover you in
thanks and make sure you get a shirt too.  I will also buy you a beer
next time we meet.

Send designs in a free file format to join@freedomboxfoundation.org
before April 15.  Vector graphics preferred.

## Get Involved!

Thanks again to everybody involved and interested in this project!
Your support is what makes this work.  If you want a more interactive
discussion than this announcement, sign up to our
[development list](http://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss).
or join us in #freedombox on oftc.net.

Best regards,
James
