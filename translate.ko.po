# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# clint <clint@debian.org>, 2011
# FULL NAME <EMAIL@ADDRESS>, 2011
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2017-12-08 00:29+0000\n"
"PO-Revision-Date: 2013-11-20 11:35+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: Korean (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/ko/)\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Title #
#, no-wrap
msgid "Worldwide community, many languages"
msgstr "세계적인 커뮤니티, 다양한 언어들"

#. type: Plain text
msgid ""
"The FreedomBox Foundation is just at the beginning of our efforts to "
"collaborate with a worldwide community.  We need to speak to everybody, "
"everywhere, in their native language.  That's why we're building translation "
"into the project from the beginning."
msgstr ""
"프리덤 박스 파운데이션은 이제 전세계에 퍼져 있는 커뮤니티들과 협력하기 위한 "
"노력을 하기 시작하였습니다. 저희는 전세계에 퍼져있는 모든 사람들과 그 사람들"
"의 언어로 소통을 하고자 합니다. 이것이 저희가 프리덤 박스의 초기 단계에서부"
"터 \"번역\"을 삽입하려고 하는 이유입니다. "

#. type: Plain text
msgid ""
"If you are interested in helping, please see <https://wiki.debian.org/"
"FreedomBox/Translate>."
msgstr ""

#~ msgid ""
#~ "We are organizing language-based groups to collaborate on high quality "
#~ "translations of project materials, both the transcribed videos and the "
#~ "various web pages and news items on the foundation's site. If you are "
#~ "interested, please [sign up to the translation email list](https://lists."
#~ "freedomboxfoundation.org/s/info/i18n) and introduce yourself.  You can "
#~ "also subscribe to that list by emailing <sympa@lists.freedomboxfoundation."
#~ "org> with `subscribe il8n` in the subject."
#~ msgstr ""
#~ "저희는 프로젝트와 관련된 자료들을 훌륭히 번역하기 위해 다양한 언어들을 사"
#~ "용하는 그룹들을 조직하고 있습니다. 홈페이지에 게시된 각종 비디오들, 방대"
#~ "한 웹 페이지들, 뉴스 보도 자료들 등이 그 대상들입니다. 만약 관심이 있으실 "
#~ "경우, [번역 이메일 리스트 싸인 업](https://lists.freedomboxfoundation.org/"
#~ "s/info/i18n) 을 해주시고, 간략한 자기소개를 부탁드립니다. 또한 'subscribe "
#~ "il8n' 를 제목으로 하는 이메일을 <sympa@lists.freedomboxfoundation.org> 로 "
#~ "전송함으로써 이 작업에 동참하실 수 있으십니다. "

#~ msgid ""
#~ "If you want to jump right in and help with translating videos, head over "
#~ "to our [media page](http://freedomboxfndn.mirocommunity.org) and click on "
#~ "the subtitle menu below any of the videos there. From that menu you will "
#~ "be able to add new translations and improve ay existing ones. If you have "
#~ "a modern browser, you can do it all from the website without having to "
#~ "install any additional software."
#~ msgstr ""
#~ "만약, 지금 당장 비디오 번역에 참여하시고자 하신다면, 저희의 [미디어 페이"
#~ "지](http://freedomboxfndn.mirocommunity.org) 를 봐주시기 바랍니다. 미디어 "
#~ "페이지로 가셔서 비디오 클립 아래에 있는 \"자막 메뉴\"를 클릭해주시기 바랍"
#~ "니다. 그 메뉴를 통해 새로운 번역자막을 첨부하실 수 있고, 이미 존재하고 있"
#~ "는 자막들을 수정하실 수도 있습니다. 만약 근래에 나온 웹 브라우저가 있으시"
#~ "면, 추가적으로 그 어떠한 소프트웨어를 깔지 않은 상태에서도, 이와 같은 작업"
#~ "을 홈페이지에서 직접 하실 수 있으십니다."
