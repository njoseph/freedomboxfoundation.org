# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-01-05 22:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title #
#, no-wrap
msgid "FreedomBox Community"
msgstr ""

#. type: Plain text
msgid ""
"The FreedomBox software is actively developed by a horizontal network of "
"contributors. This horizontal network continues to make tremendous progress "
"in software development, often without the guidance of the FreedomBox "
"Foundation. The community, therefor, manages a separate website where all "
"software updates are posted and available for download for free. In "
"addition, content related to FreedomBox is hosted on that website, such as "
"educational introductions to our technology, user guides, and promotional "
"content. Please be sure to check them out [here](https://freedombox.org/)."
msgstr ""

#. type: Plain text
msgid ""
"In addition to managing a separate website, the FreedomBox community also "
"uses a wiki hosted on the Debian website to document software development, "
"coordinate goals and activities, and post blueprints of future content for "
"the FreedomBox website. The wiki also hosts a large body of helpful "
"material, including archived user support, FAQ's, and a user manuals "
"translated into multiple languages. The FreedomBox Foundation in "
"coordination with the FreedomBox community continue to transfer the most "
"essential material from the wiki to the FreedomBox website."
msgstr ""
